import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrder {
    private JLabel topLabel;
    private JButton tempraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JButton checkOutButton;
    private JLabel totalprice;
    private JTextPane textPane1;
    private JPanel root;
    int price=0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrder");
        frame.setContentPane(new FoodOrder().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            String t=textPane1.getText();
            price+=100;
            totalprice.setText(price+"yen");
            textPane1.setText(t+"\n"+food);
            JOptionPane.showMessageDialog(null,"Thank you ordering  "+food+"! It will be served as soon as possible.");
        }
    }
    public FoodOrder() {
        tempraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempra");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation==0) {
                    JOptionPane.showMessageDialog(null, "Thank you.The total prices is" + price + " yen.");
                    textPane1.setText("");
                    totalprice.setText("0 yen");
                    price = 0;
                }
            }
        });
    }
}
